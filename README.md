# simple-csv-parser README #

---

### What is this repository for? ###

* This project is a simple experiment to look at how to use Try/Success/Failure when parsing CSV strings into case class instances.
* It also includes some basic tests implemented with [ScalaTest](http://www.scalatest.org/) and [ScalaCheck](https://www.scalacheck.org/).

---
### How do I get set up? ###

* Clone the repository to your local machine.
* You just need [Scala](http://www.scala-lang.org/) and [SBT](http://www.scala-sbt.org/) to run these programs.
* You can also get these as part of the [Activator tool](https://www.lightbend.com/community/core-tools/activator-and-sbt) from [Lightbend](https://www.lightbend.com/) (formerly Typesafe).
* The `build.sbt` file specifies the dependencies for the project.
---
### How do I run the application?
* Navigate to the root directory of the project e.g.:  `cd simple-csv-parser`
* Run the CSVParsingDemo App with SBT to see sample output on your console for some dummy data:  `sbt run`

---
### How do I test it?
* The download includes some ScalaTest unit tests and some ScalaCheck property tests.
* You can run all of these via SBT:  `sbt test`

---
