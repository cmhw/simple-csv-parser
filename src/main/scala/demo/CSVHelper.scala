package demo

import scala.util.{Try, Success, Failure}

object CSVHelper {

  // 3 String fields to hold incoming CSV data
  case class FubarStr(a: String, b: String, c: String)

  // 3 Double fields to hold correctly formatted/typed data
  case class Fubar(a: Double, b: Double, c: Double)

  // Define an exception for records with wrong no of fields
  case class WrongNumberOfFields(message: String) extends Exception(message)

  // Parser functions using Try to cope with Success/failure
  // (could do this in a single parse step but this gives us more flexibility)

  // Could include a check that CSV string produces correct no of fields:
  def parseToFubarStrWithFieldCheck(data: String, expectedFields: Int = 0): Try[FubarStr] = {
    val ss = data.split(",")
    if (ss.length != expectedFields)
      new Failure(WrongNumberOfFields(s"[$data] has wrong number of fields: ${ss.length}. Should be: $expectedFields."))
    else
      Try(FubarStr(ss(0), ss(1), ss(2)))
  }

  // No check on number of fields e.g. we just want the first 3 even if there are more
  def parseToFubarStr(data: String): Try[FubarStr] = {
    val ss = data.split(",")
    Try(FubarStr(ss(0), ss(1), ss(2)))
  }

  def parseToFubar(data: FubarStr): Try[Fubar] = {
    Try(Fubar(data.a.toDouble, data.b.toDouble, data.c.toDouble))
  }

  // Combined parser including check that CSV string produces correct no of fields:
  def combinedParseWithFieldCheck(data: String, expectedFields: Int = 0): Try[Fubar] = {
    parseToFubarStrWithFieldCheck(data, expectedFields)
    match {
      case Success(fbs) => parseToFubar(fbs)
      case Failure(ex) => Failure(ex)
    }
  }

}
