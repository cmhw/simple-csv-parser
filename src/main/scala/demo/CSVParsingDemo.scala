package demo

object CSVParsingDemo extends App {
  /*
   * Ad hoc CSV parsing that throws away badly formatted data.
   * Uses Try to wrap parsing so we can filter out Successes.
   */

  import scala.util.{Try, Success, Failure}
  import CSVHelper._

   // Dummy CSV data
  // CSV data
  val raw: Seq[String] = Vector("1,2,3", // good
    "1,2,*", // good FubarStr, but bad Fubar - expect number but receive character
    "1,2", // bad - not enough fields
    "2,4,6", // good Fubar
    "TOTAL#RUBBISH", // bad - not enough fields, wrong format
    "TOO,MANY,FIELDS,IN,THIS,1") // bad - too many fields

  println("==========================")
  println("Raw CSV data")
  println("==========================")
  raw.foreach(println)

  // Process each row from CSV data in turn:
  // We only want the valid instances so we only care about Success values.

  // Read CSV strings and get the successfully parsed FubarStr instances only
  val fubarStrs = raw.map(parseToFubarStr(_)).filter(_.isSuccess).map(_.get)

  // How did we do?
  println("==========================")
  println("FubarStrs - no field check")
  println("==========================")
  fubarStrs.foreach(println)

  // Alternatively, we might want to check the CSV has the right number of fields
  val requiredFields = 3
  val checkedFubarStrs = raw.map(parseToFubarStrWithFieldCheck(_, requiredFields)).filter(_.isSuccess).map(_.get)
  // How did we do?
  println("============================")
  println("FubarStrs - with field check")
  println("============================")
  checkedFubarStrs.foreach(println)
  // Now convert FubarStr to Fubar instances

  // Read FubarStr instances and get the successfully parsed Fubar instances only
  val fubars = checkedFubarStrs.map(parseToFubar(_)).filter(_.isSuccess).map(_.get)
  // How did we do?
  println("============================")
  println("Fubars")
  println("============================")
  fubars.foreach(println)

  /*******************************************************
  // And if you really do want to know about the failures:
  *******************************************************/

  // Run the combined parser with field check on the raw CSV data
  // and split the results into Successes and Failures
  val (good, bad) = raw.map(combinedParseWithFieldCheck(_, requiredFields)).partition(_.isSuccess)

  println("============================")
  println("Good CSV results (Successes)")
  println("============================")
  good.foreach(println)

  println("============================")
  println("Bad CSV results (Failures)")
  println("============================")
  bad.foreach(println)
}
