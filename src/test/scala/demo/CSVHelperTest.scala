package demo

import demo.CSVHelper.{Fubar, FubarStr, WrongNumberOfFields}
import org.scalatest.{Matchers, WordSpecLike}

import scala.util.{Failure, Success}

class CSVHelperTest extends WordSpecLike with Matchers {

  "A numerically valid CSV record with too many fields" when {
    val data = "1,2.0,-3,4,5,6"
    val requiredFields = 3

    "parsed with the field checker parseToFubarStrWithFieldCheck" should {
      val result = CSVHelper.parseToFubarStrWithFieldCheck(data, requiredFields)

      "return a Failure(WrongNumberOfFields)" in {
        assert(result match {
          case Failure(WrongNumberOfFields(msg)) => true
          case _ => false
        })

      }
    }
    "parsed without the field checker using parseToFubarStr" should {
      val expected = FubarStr("1", "2.0", "-3")
      val result = CSVHelper.parseToFubarStr(data)

      "return a Success(FubarStr) with the leading fields" in {
        assert(result == Success(expected))
      }
    }
    "parsed with the combined parser/checker combinedParseWithFieldCheck" should {
      val result = CSVHelper.combinedParseWithFieldCheck(data, requiredFields)

      "return a Failure(WrongNumberOfFields)" in {
        assert(result match {
          case Failure(WrongNumberOfFields(msg)) => true
          case _ => false
        })

      }
    }

  }

  "A numerically invalid CSV record with too many fields" when {
    val data = "1,ABC,3,DEF,5,6"
    val requiredFields = 3

    "parsed with the field checker parseToFubarStrWithFieldCheck" should {
      val result = CSVHelper.parseToFubarStrWithFieldCheck(data, requiredFields)

      "return a Failure(WrongNumberOfFields exception)" in {
        assert(result match {
          case Failure(WrongNumberOfFields(msg)) => true
          case _ => false
        })

      }
    }

    "parsed with the combined parser/checker combinedParseWithFieldCheck" should {
      val result = CSVHelper.combinedParseWithFieldCheck(data, requiredFields)

      "return a Failure(WrongNumberOfFields)" in {
        assert(result match {
          case Failure(WrongNumberOfFields(msg)) => true
          case _ => false
        })

      }
    }

    "parsed without the field checker using parseToFubarStr" should {
      val expected = FubarStr("1", "ABC", "3")
      val result = CSVHelper.parseToFubarStr(data)

      "return a Success(FubarStr) with the leading fields" in {
        assert(result == Success(expected))
      }
    }
  }

  "A numerically invalid CSV record with too few fields" when {
    val data = "1,ABC"
    val requiredFields = 3

    "parsed with the field checker parseToFubarStrWithFieldCheck" should {
      val result = CSVHelper.parseToFubarStrWithFieldCheck(data, requiredFields)

      "return a Failure(WrongNumberOfFields)" in {
        assert(result match {
          case Failure(WrongNumberOfFields(msg)) => true
          case _ => false
        })

      }
    }

    "parsed with the combined parser/checker combinedParseWithFieldCheck" should {
      val result = CSVHelper.combinedParseWithFieldCheck(data, requiredFields)

      "return a Failure(WrongNumberOfFields)" in {
        assert(result match {
          case Failure(WrongNumberOfFields(msg)) => true
          case _ => false
        })

      }
    }

    "parsed without the field checker using parseToFubarStr" should {

      val result = CSVHelper.parseToFubarStr(data)

      "return a Failure(java.lang.ArrayIndexOutOfBoundsException)" in {
        assert(result match {
          case Failure(ex: java.lang.ArrayIndexOutOfBoundsException) => true
          case _ => false
        })

      }
    }
  }


  "A numerically valid CSV record with correct number of fields" when {
    val data = "1.0,2,-3.0"
    val requiredFields = 3

    "parsed with the field checker parseToFubarStrWithFieldCheck" should {
      val expected = FubarStr("1.0", "2", "-3.0")
      val result = CSVHelper.parseToFubarStrWithFieldCheck(data, requiredFields)

      "return a Success(FubarStr)" in {
        assert(result == Success(expected))
      }
    }
    "parsed without the field checker using parseToFubarStr" should {
      val expected = FubarStr("1.0", "2", "-3.0")
      val result = CSVHelper.parseToFubarStr(data)

      "return a Success(FubarStr)" in {
        assert(result == Success(expected))
      }
    }
    "parsed with the combined parser/checker combinedParseWithFieldCheck" should {
      val result = CSVHelper.combinedParseWithFieldCheck(data, requiredFields)
      val expected = Fubar(1,2,-3)

      "return a Success(Fubar)" in {
        assert(result == Success(expected))
      }
    }

  }

  "A numerically invalid CSV record with correct number of fields" when {
    val data = "1,X,3"
    val requiredFields = 3

    "parsed with the field checker parseToFubarStrWithFieldCheck" should {
      val expected = FubarStr("1", "X", "3")
      val result = CSVHelper.parseToFubarStrWithFieldCheck(data, requiredFields)

      "return a Success(FubarStr)" in {
        assert(result == Success(expected))
      }
    }
    "parsed without the field checker using parseToFubarStr" should {
      val expected = FubarStr("1", "X", "3")
      val result = CSVHelper.parseToFubarStr(data)

      "return a Success(FubarStr)" in {
        assert(result == Success(expected))
      }
    }
    "parsed with the combined parser/checker combinedParseWithFieldCheck" should {
      val result = CSVHelper.combinedParseWithFieldCheck(data, requiredFields)

      "return a Failure(java.lang.NumberFormatException)" in { assert(result match {
        case Failure(ex: java.lang.NumberFormatException) => true
        case _ => false
      })
      }
    }

  }
}