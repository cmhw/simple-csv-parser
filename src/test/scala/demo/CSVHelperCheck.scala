package demo

import java.math.BigInteger

import demo.CSVHelper.{Fubar, WrongNumberOfFields, FubarStr}
import org.scalacheck.{Prop, Properties}

import scala.util.{Failure, Success}

object CSVHelperCheck extends Properties("Testing CSVHelper with various input strings: \n") {
  /*
   * Using ScalaCheck to try different strings and numbers against the CSVHelper.
   * Want to make sure that valid numbers (Ints or Doubles) will pass, while any
   * invalid values will result in one of the expected Failure values i.e. a specific
   * Java exception or our own custom exception.
   */

  property("Parse 3 strings to FubarStr (not checking field number)\n") = Prop.forAll {
    (s1: String, s2: String, s3: String) =>
      val csv = List(s1, s2, s3) mkString ","
      CSVHelper.parseToFubarStr(csv) match {
        case Success(fbs: FubarStr) => true
        case Failure(ex: java.lang.ArrayIndexOutOfBoundsException) => true
        case _ => false
      }
  }

  property("Parse 3 strings to FubarStr (checking field number)\n") = Prop.forAll {
    (s1: String, s2: String, s3: String) =>
      val csv = List(s1, s2, s3) mkString ","
      CSVHelper.parseToFubarStrWithFieldCheck(csv, 3) match {
        case Success(fbs: FubarStr) => true
        case Failure(WrongNumberOfFields(msg)) => true
        case _ => false
      }
  }

  property("Parse 3 strings to Fubar (checking field number)\n") = Prop.forAll {
    (s1: String, s2: String, s3: String) =>
      val csv = List(s1, s2, s3) mkString ","
      CSVHelper.combinedParseWithFieldCheck(csv, 3) match {
        case Success(fb: Fubar) => true
        case Failure(WrongNumberOfFields(msg)) => true
        case Failure(ex: java.lang.NumberFormatException) => true
        case _ => false
      }
  }

  property("Parse arbitrary string to FubarStr (not checking field number)\n") = Prop.forAll {
    (csv: String) =>
      CSVHelper.parseToFubarStr(csv) match {
        case Success(fbs: FubarStr) => true
        case Failure(ex: java.lang.ArrayIndexOutOfBoundsException) => true
        case _ => false
      }
  }

  property("Parse arbitrary string to FubarStr (checking field number)\n") = Prop.forAll {
    (csv: String) =>
      CSVHelper.parseToFubarStrWithFieldCheck(csv, 3) match {
        case Success(fbs: FubarStr) => true
        case Failure(WrongNumberOfFields(msg)) => true
        case _ => false
      }
  }

  property("Parse arbitrary string to Fubar (checking field number)\n") = Prop.forAll {
    (csv: String) =>
      CSVHelper.combinedParseWithFieldCheck(csv, 3) match {
        case Success(fb: Fubar) => true
        case Failure(WrongNumberOfFields(msg)) => true
        case Failure(ex: java.lang.NumberFormatException) => true
        case _ => false
      }
  }

  property("Parse 3 Int strings to Fubar (checking field number)\n") = Prop.forAll {
    (n1: Int, n2: Int, n3: Int) =>
      val csv = List(n1.toString, n2.toString, n3.toString) mkString ","
      // any combination of 3 Ints should be OK
      CSVHelper.combinedParseWithFieldCheck(csv, 3) match {
        case Success(fb: Fubar) => true
        case _ => false
      }
  }

  property("Parse 3 Double strings to Fubar (checking field number)\n") = Prop.forAll {
    (n1: Double, n2: Double, n3: Double) =>
      val csv = List(n1.toString, n2.toString, n3.toString) mkString ","
      // any combination of 3 Doubles should be OK
      CSVHelper.combinedParseWithFieldCheck(csv, 3) match {
        case Success(fb: Fubar) => true
        case _ => false
      }
  }

  property("Parse 3 Float strings to Fubar (checking field number)\n") = Prop.forAll {
    (n1: Float, n2: Float, n3: Float) =>
      val csv = List(n1.toString, n2.toString, n3.toString) mkString ","
      // any combination of 3 Ints should be OK
      CSVHelper.combinedParseWithFieldCheck(csv, 3) match {
        case Success(fb: Fubar) => true
        case _ => false
      }
  }

}
